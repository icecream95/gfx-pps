#pragma once

#include <string>
#include <optional>


namespace pps::weston
{

using WestonId = uint32_t;


struct WestonTimepoint
{
	enum class Type
	{
		UNDEFINED,
		// Client submitting a new frame
		CORE_COMMIT_DAMAGE,
		CORE_FLUSH_DAMAGE,
		CORE_REPAINT_BEGIN,
		CORE_REPAINT_END,
		CORE_KMS_FLIP_BEGIN,
		CORE_KMS_FLIP_END,
		CORE_REPAINT_ENTER_LOOP,
		CORE_REPAINT_EXIT_LOOP,
		// Repaint has been requested
		CORE_REPAINT_REQ_BEGIN,
		CORE_REPAINT_REQ_END,
		CORE_REPAINT_DELAY_BEGIN,
		CORE_REPAINT_DELAY_END,
		RENDERER_GPU_BEGIN,
		RENDERER_GPU_END,
		VBLANK_BEGIN,
		VBLANK_END,
		MAX,
	};

	uint64_t get_timestamp() const { return ts.tv_sec * 1000000000 + ts.tv_nsec; }

	timespec ts = {};

	std::string name = "";

	Type type = Type::UNDEFINED;

	WestonId output = 0;

	WestonId surface = 0;
};


struct WestonObject
{
	enum class Type
	{
		UNDEFINED,
		SURFACE,
		OUTPUT,
		MAX
	};

	WestonId id = 0;

	Type type = Type::UNDEFINED;

	std::string name;
	std::string description;
	
	/// Since not always a commit damage is followed by a flush we store last
	/// commit damage of a surface and only generate a slice when we get a flush.
	/// If we instead get another commit, we generate an instant event for
	/// the previous commit, and overwrite last commit with the new one.
	std::optional<WestonTimepoint> last_damage;

	/// Keep track of repaint required timepoint, so we can consume it when repaint finishes
	bool repaint_required = false;

	/// Kep track of repaint delay, so we can consue it when repaint starts again
	bool repaint_delay = false;

	/// @return Whether two weston objects have the same ID
	bool operator==( const WestonObject& oth ) const { return id == oth.id; }
};


std::string to_string( const WestonObject& wo );


} // namespace pps::weston
