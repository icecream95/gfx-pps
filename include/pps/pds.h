/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <perfetto.h>

namespace pps
{

enum class State
{
	Stop,  // initial state, or stopped by the tracing service
	Start, // running, sampling data
};


/// @brief Checks whether a return value is valid
/// @param res Result from a syscall
/// @param msg Message to prepend to strerror
/// @return True if ok, false otherwise
bool check( int res, const char* msg );


}  // namespace pps
