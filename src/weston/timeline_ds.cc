/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Aguado Puig, Quim <quim.aguado@collabora.com>
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include "weston/timeline_ds.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>

#include "pps/algorithm.h"


namespace pps::weston
{

State TimelineDataSource::state = State::Stop;

const char* TimelineDataSource::read_file = nullptr;

void TimelineDataSource::reset()
{
	if ( stream )
	{
		weston_debug_stream_v1_destroy( stream );
		stream = nullptr;
		PERFETTO_ILOG( "Stream %s destroyed", get_stream_name() );
	}

	if ( display )
	{
		// Wait for server to close all files
		wl_display_roundtrip( display );

		if ( registry )
		{
			wl_registry_destroy( registry );
			registry = nullptr;
			PERFETTO_ILOG( "Registry destroyed" );
		}

		wl_display_disconnect( display );
		display = nullptr;
		PERFETTO_ILOG( "Display disconnected" );
	}

	if ( write_fd >= 0 )
	{
		close( write_fd );
		write_fd = -1;
	}

	if ( read_fd >= 0 )
	{
		close( read_fd );
		read_fd = -1;
	}

	json_buffer.clear();
	parser = Parser();
	parent_tracks.clear();
	tracks.clear();

	state = State::Stop;
}


TimelineBuffer read_buffer( const int fd )
{
	assert( fd >= 0 && "File should be open" );

	auto buffer = TimelineBuffer( 512 );
	ssize_t read_count = 0;

	struct pollfd fds[1];
	fds[0].fd = fd;
	fds[0].events = POLLIN;
	fds[0].revents = 0;

	int retpoll = poll( fds, 1, 0 );
	if ( retpoll < 0 )
	{
		PERFETTO_FATAL( "Poll failed" );
	}
	if ( retpoll == 0 )
	{
		// Timeout
	}
	else if ( ( retpoll & POLLIN ) == 0 )
	{
		// There is not data to read
	}
	else
	{
		read_count = read( fd, buffer.data(), buffer.size() - 1 );

		if ( read_count < 0 )
		{
			PERFETTO_FATAL( "Error reading pipe" );
		}
	}

	// Mark end of string
	buffer[read_count] = 0;

	if ( read_count > 0 )
	{
		// Get rid of unused memory
		buffer.resize( read_count + 1 );
	}
	else
	{
		buffer.clear();
	}
	
	return buffer;
}


/// @brief A track event type can be slice begin or slice end.
/// We do not use instant events as perfetto does not support them yet.
/// @return The event type according to the weston timepoint
perfetto::protos::pbzero::TrackEvent::Type
track_from_timepoint( WestonTimepoint::Type type )
{
	using namespace perfetto::protos::pbzero;

	switch ( type )
	{
	case WestonTimepoint::Type::CORE_REPAINT_REQ_BEGIN:
	case WestonTimepoint::Type::CORE_REPAINT_DELAY_BEGIN:
	case WestonTimepoint::Type::CORE_COMMIT_DAMAGE:
	case WestonTimepoint::Type::CORE_REPAINT_BEGIN:
	case WestonTimepoint::Type::CORE_REPAINT_ENTER_LOOP:
	case WestonTimepoint::Type::CORE_KMS_FLIP_BEGIN:
	case WestonTimepoint::Type::RENDERER_GPU_BEGIN:
	case WestonTimepoint::Type::VBLANK_BEGIN:
		return TrackEvent::TYPE_SLICE_BEGIN;
	case WestonTimepoint::Type::CORE_REPAINT_REQ_END:
	case WestonTimepoint::Type::CORE_REPAINT_DELAY_END:
	case WestonTimepoint::Type::CORE_FLUSH_DAMAGE:
	case WestonTimepoint::Type::CORE_REPAINT_END:
	case WestonTimepoint::Type::CORE_REPAINT_EXIT_LOOP:
	case WestonTimepoint::Type::CORE_KMS_FLIP_END:
	case WestonTimepoint::Type::RENDERER_GPU_END:
	case WestonTimepoint::Type::VBLANK_END:
		return TrackEvent::TYPE_SLICE_END;
	default:
		PERFETTO_ELOG( "Timepoint type not handled: %d", static_cast<int32_t>( type ) );
		return TrackEvent::TYPE_UNSPECIFIED;
	}
}


void TimelineDataSource::send_track_descriptor(
	const Track& track,
	const Track* parent )
{
	// Send track descriptor for this parent if not already sent
	if ( parent && !CONTAINS( parent_tracks, parent->id ) )
	{
		auto packet = ctx->NewTracePacket();
		packet->set_timestamp( perfetto::base::GetBootTimeNs().count() );

		PERFETTO_LOG( "Parent %lu - %s", parent->id, parent->name.c_str() );

		auto track = packet->set_track_descriptor();
		track->set_name( parent->name );
		track->set_uuid( parent->id );

		// Each weston output will be a process-track.
		// This is useful to organize events on children thread-tracks.
		auto process = track->set_process();
		process->set_process_name( parent->name );
		process->set_pid( parent->id );

		parent_tracks.emplace_back( parent->id );
	}

	// Create track for this object if not already there
	if ( CONTAINS( tracks, track.id ) )
	{
		return; // Already sent
	}

	auto packet = ctx->NewTracePacket();
	packet->set_timestamp( perfetto::base::GetBootTimeNs().count() );

	auto track_desc = packet->set_track_descriptor();
	PERFETTO_LOG(
		"Parent %lu - Track %lu - %s",
		parent ? parent->id : 0,
		track.id, track.name.c_str()
	);
	track_desc->set_name( track.name );
	track_desc->set_uuid( track.id );

	if ( parent )
	{
		// If this event has an output, this track will be a thread-track
		// which parent is a process-track related to that output
		track_desc->set_parent_uuid( parent->id );
		auto thread = track_desc->set_thread();
		thread->set_pid( parent->id );
		thread->set_thread_name( track.name );
		thread->set_tid( track.id );
	}

	tracks.emplace_back( track.id );
}


void TimelineDataSource::send_track_event(
	const WestonTimepoint& event,
	perfetto::protos::pbzero::TrackEvent::Type type )
{
	using namespace perfetto::protos::pbzero;

	auto output = parser.get_object( event.output );
	if ( !output )
	{
		PERFETTO_FATAL( "Event %s does not refer to an output", event.name.c_str() );
	}

	auto parent_track = get_parent_track( *output );

	// Send the Vblank track
	auto vblank_track = get_vblank_track( *output );
	send_track_descriptor( vblank_track, &parent_track );

	// Send the repaint cycle track
	auto repaint_track = get_track( *output );
	send_track_descriptor( repaint_track, &parent_track );

	// Send the repaint requested track
	auto repaint_req_track = get_repaint_req_track( *output );
	send_track_descriptor( repaint_req_track, &parent_track );

	// Send the track descriptor for the surface (if there is one)
	auto surface = parser.get_object( event.surface );
	auto track = get_track( *output, surface );
	send_track_descriptor( track, &parent_track );

	// Put repaint req async events in their own track
	if ( event.type == WestonTimepoint::Type::CORE_REPAINT_REQ_BEGIN ||
		event.type == WestonTimepoint::Type::CORE_REPAINT_REQ_END )
	{
		track = repaint_req_track;
	}

	// Put vlank events in their own track
	if ( event.type == WestonTimepoint::Type::VBLANK_BEGIN ||
		event.type == WestonTimepoint::Type::VBLANK_END )
	{
		track = vblank_track;
	}

	track.send(
		event,
		type == TrackEvent::TYPE_INSTANT ? TrackEvent::TYPE_SLICE_BEGIN : type,
		ctx->NewTracePacket() );

	// Second packet as workaround to instant events not being
	// properly handled by perfetto ui
	/// @todo Remove the workaround once Perfetto UI gets fixed
	if ( type == TrackEvent::TYPE_INSTANT )
	{
		// Create a copy of the timepoint
		WestonTimepoint end_event = event;
		// Duration of the slice will be 1 us
		end_event.ts.tv_nsec += 10000;

		// Send another packet with slice end
		track.send(
			end_event,
			TrackEvent::TYPE_SLICE_END,
			ctx->NewTracePacket() );
	}
}


void TimelineDataSource::send_packet( WestonTimepoint& event )
{
	using namespace perfetto::protos::pbzero;
	switch ( event.type )
	{
	// Damage commit special case
	case WestonTimepoint::Type::CORE_COMMIT_DAMAGE:
	{
		auto surface = parser.get_object( event.surface );
		assert( surface && "Commit damage does not refer to a surface" );

		if ( surface->last_damage )
		{
			// Previous commit damage becomes an instant event
			send_track_event( *surface->last_damage, TrackEvent::TYPE_INSTANT );
		}

		// Overwrite last damage with new one and continue
		surface->last_damage.emplace( std::move( event ) );
		break;
	}
	// Flush damage special case
	case WestonTimepoint::Type::CORE_FLUSH_DAMAGE:
	{
		auto surface = parser.get_object( event.surface );
		assert( surface && "Flush damage does not refer to a surface" );

		if ( surface->last_damage )
		{
			// Make a slice with both, consume commit and continue
			// Associate last damage to the output this flush refers to
			surface->last_damage->output = event.output;
			auto type = track_from_timepoint( surface->last_damage->type );
			send_track_event( *surface->last_damage, type );
			/// @todo What happens to flushes coming from other outputs?
			surface->last_damage.reset();

			type = track_from_timepoint( event.type );
			send_track_event( event, type );
		}
		else
		{
			// No commit to associate with
			send_track_event( event, TrackEvent::TYPE_INSTANT );
		}
		break;
	}
	case WestonTimepoint::Type::RENDERER_GPU_BEGIN:
	case WestonTimepoint::Type::RENDERER_GPU_END:
	{
		/// @todo These timepoints are not working properly at the moment
		///       Just remove this case when they got fixed
		break;
	}
	default:
	{
		auto type = track_from_timepoint( event.type );
		send_track_event( event, type );
		break;
	}
	} // switch
}


std::vector<WestonTimepoint> TimelineDataSource::read_timepoints( const int read_fd )
{
	std::vector<WestonTimepoint> timepoints;

	while ( true )
	{
		// Read bytes from the file descriptor
		auto byte_buffer = read_buffer( read_fd );
		if ( byte_buffer.empty() )
		{
			break; // Nothing to do
		}

		// Append to the current json buffer, as it may have previous unparsed bytes
		json_buffer.reserve( json_buffer.size() + byte_buffer.size() );
		APPEND( json_buffer, byte_buffer );

		auto [events, chars_parsed] = parser.parse( json_buffer );
		APPEND( timepoints, events );

		// Remove parsed chars and '0'
		// Unparsed characters will be reused on the next iteration
		std::rotate(
			std::begin( json_buffer ),
			std::begin( json_buffer ) + chars_parsed,
			std::end( json_buffer ) );
		json_buffer.resize( json_buffer.size() - chars_parsed - 1 );
	}

	return timepoints;
}


void TimelineDataSource::sample()
{
	assert( ctx && "Tracing context is not valid" );

	if ( !read_file )
	{
		// Process incoming events before attempting to read them
		assert( display && "Display should be initialized" );
		wl_display_roundtrip( display );
	}

	for ( auto& timepoint : read_timepoints( read_fd ) )
	{
		send_packet( timepoint );
	}
}


void TimelineDataSource::debug_available( void* data, weston_debug_v1* debug, const char* name, const char* description )
{
	auto datasource = static_cast<TimelineDataSource*>( data );

	if ( strcmp( name, get_stream_name() ) == 0 )
	{
		datasource->stream_exists = true;
		PERFETTO_ILOG( "Debug stream %s found", name );
	}
}


void TimelineDataSource::global_add( void* data, wl_registry* registry, uint32_t id, const char* interface, uint32_t version )
{
	if ( strcmp( interface, weston_debug_v1_interface.name ) == 0 )
	{
		auto datasource = static_cast<TimelineDataSource*>( data );
		assert( !datasource->debug_iface && "Debug interface should not be initialized" );
		datasource->debug_iface = static_cast<weston_debug_v1*>(
			wl_registry_bind( registry, id, &weston_debug_v1_interface, version ) );
		weston_debug_v1_add_listener( datasource->debug_iface, &datasource->debug_listener, datasource );
	}
}


void TimelineDataSource::global_remove( void *data, wl_registry* registry, uint32_t id )
{
	/// @todo Fail if debug interface is removed from registry
}


void TimelineDataSource::OnSetup( const SetupArgs& args )
{
	if ( read_file )
	{
		// Do not connect to wayland server when reading from file
		return;
	}

	/// @todo Get sample period from args.config
	assert( !display && "Display should not be initialized" );
	display = wl_display_connect( nullptr );
	if ( !display )
	{
		PERFETTO_FATAL( "Failed to create Wayland display" );
	}

	auto version = wl_display_get_version( display );
	PERFETTO_LOG( "Connected to Wayland server v%u", version );

	assert( !registry && "Registry should not be initialized" );
	registry = wl_display_get_registry( display );
	wl_registry_add_listener( registry, &registry_listener, this );

	wl_display_roundtrip( display );

	if ( !debug_iface )
	{
		PERFETTO_FATAL( "Global %s does not exists in the registry. "
			"Make sure weston is running with the --debug flag",
			weston_debug_v1_interface.name );
	}

	wl_display_roundtrip( display );

	if ( !stream_exists )
	{
		PERFETTO_FATAL( "Stream %s not found", get_stream_name() );
	}
	
	PERFETTO_LOG( "Initialization finished" );
}


void TimelineDataSource::handle_stream_complete( void* data, weston_debug_stream_v1* stream )
{
	auto datasource = static_cast<TimelineDataSource*>( data );
	datasource->stream_complete = true;
	weston_debug_stream_v1_destroy( stream );
	stream = nullptr;
	PERFETTO_FATAL( "Stream %s completed", get_stream_name() );
}


void TimelineDataSource::handle_stream_failure( void* data, weston_debug_stream_v1* stream, const char* msg )
{
	auto datasource = static_cast<TimelineDataSource*>( data );
	datasource->stream_complete = true;
	weston_debug_stream_v1_destroy( stream );
	stream = nullptr;
	PERFETTO_FATAL( "Stream %s failed", get_stream_name() );
}


void TimelineDataSource::OnStart( const StartArgs& args )
{
	PERFETTO_ILOG( "Starting %s stream", get_stream_name() );

	assert( read_fd == -1 && "Read pipe should not be initialized at this point" );
	assert( write_fd == -1 && "Write pipe should not be initialized at this point" );

	if ( read_file )
	{
		// Open the file for reading
		read_fd = open( read_file, O_RDONLY );
		if ( read_fd < 0 )
		{
			PERFETTO_FATAL( "Cannot open file for reading" );
		}
	}
	else
	{
		// Create a pipe for interprocess communication
		int fd[2];
		if ( pipe( fd ) < 0 )
		{
			PERFETTO_FATAL( "Can not create a pipe to read the stream" );
		}
		PERFETTO_LOG( "Created stream pipe" );
		read_fd = fd[0];
		write_fd = fd[1];

		assert( stream == nullptr && "Stream should not be initialized at this point" );
		assert( debug_iface && "Debug interface should be valid" );

		// Weston debug protocol will dump onto the write end of the pipe
		stream = weston_debug_v1_subscribe( debug_iface, get_stream_name(), write_fd );
		// While this data-source will read the data as it comes
		weston_debug_stream_v1_add_listener( stream, &stream_listener, nullptr );

		weston_debug_v1_destroy( debug_iface );
		debug_iface = nullptr;
	}

	state = State::Start;
	PERFETTO_LOG( "OnStart finished" );
}


void TimelineDataSource::OnStop( const StopArgs& args )
{
	PERFETTO_LOG( "Parse errors: %lu", parser.get_error_count() );
	state = State::Stop;
	auto stop_closure = args.HandleStopAsynchronously();
	Trace( [] ( TraceContext ctx ) {
		PERFETTO_LOG( "Tracing lambda called while stopping" );
		ctx.Flush();
	} );
	stop_closure();
	reset();
	PERFETTO_LOG( "OnStop finished" );
}


void TimelineDataSource::trace_callback( TraceContext ctx )
{
	if ( auto data_source = ctx.GetDataSourceLocked() )
	{
		data_source->ctx = &ctx;
		data_source->sample();
		data_source->ctx = nullptr;

		if ( read_file )
		{
			// When reading from a file, the data source sends all the packets in one go
			// Therefore once finished, it just stops "tracing".
			state = State::Stop;
		}
	}
	else
	{
		PERFETTO_FATAL( "Cannot get data source from Perfetto context" );
	}
	
}


} // namespace pps::weston
